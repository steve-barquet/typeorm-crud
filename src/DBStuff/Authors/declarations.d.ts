export interface IAuthors {
  name: string;
  email: string;
}

export interface IAuthorsWithId {
  id: number;
  name?: string;
  email?: string;
}
