// ---Types
import { IAuthors, IAuthorsWithId } from './declarations';
// ---DB
import { authorsRepo } from '../configuration';

// ------------------------------Authors----------------------------------------------------
async function insertAuthor(incommingAuthor: IAuthors) {
  try {
    const newAuthor = authorsRepo.create(incommingAuthor);
    const saveOeration = await authorsRepo.save(newAuthor);
    return {
      isError: false,
      data: saveOeration
    };
  } catch (error) {
    return {
      isError: true,
      data: error
    };
  }
}

async function readAuthor() {
  try {
    const allAuthors = await authorsRepo.find();
    return {
      isError: false,
      data: allAuthors
    };
  } catch (error) {
    return {
      isError: true,
      data: error
    };
  }
}

async function editAuthor(incommingAuthor: IAuthorsWithId) {
  try {
    let targetAuthor = await authorsRepo.findOneBy({ id: incommingAuthor.id });

    if (targetAuthor && targetAuthor.name) {
      targetAuthor = { ...targetAuthor, ...incommingAuthor };
      const updatedAuthor = await authorsRepo.save(targetAuthor);
      return {
        isError: false,
        data: updatedAuthor
      };
    }
    throw new Error('No se encontró el autor');
  } catch (error) {
    return {
      isError: true,
      data: error
    };
  }
}
async function deleteAuthor(id: number) {
  try {
    const result = await authorsRepo.delete(id);
    return {
      isError: false,
      data: result
    };
  } catch (error) {
    return {
      isError: true,
      data: error
    };
  }
}
export const authorsDB = {
  getAll: readAuthor,
  saveOne: insertAuthor,
  updateOne: editAuthor,
  deleteOne: deleteAuthor
};
