import { IAuthors } from './declarations';
import { authorsDB } from './operations';

export const authorsEx: IAuthors[] = [
  {
    name: 'Baki Hanma',
    email: 'baki@baki.com'
  },
  {
    name: 'Jessie Pinkman',
    email: 'jessie@baki.com'
  },
  {
    name: 'Charles Xavier',
    email: 'xavier@xmen.com'
  }
];
async function justPrint() {
  console.log('\n---AuthorPosts---: ', await authorsDB.getAll());
}

async function insertAndPrint() {
  console.log('\n---AuthorPosts---: ', await authorsDB.getAll());
  const insertion = await authorsDB.saveOne(authorsEx[4]);
  console.log('\ninsertion: ', insertion);
  console.log('\n---AuthorPosts---: ', await authorsDB.getAll());
}

async function editAndPrint() {
  console.log('\n---AuthorPosts---: ', await authorsDB.getAll());
  const update = await authorsDB.updateOne({ id: 2, name: 'Naruto Uzumaki' });
  console.log('\ninsertion: ', update);
  console.log('\n---AuthorPosts---: ', await authorsDB.getAll());
}

async function deleteAndPrint() {
  console.log('\n---AuthorPosts---: ', await authorsDB.getAll());
  const update = await authorsDB.deleteOne(2);
  console.log('\ninsertion: ', update);
  console.log('\n---AuthorPosts---: ', await authorsDB.getAll());
}

export const autorTest = {
  justPrint,
  insertAndPrint,
  editAndPrint,
  deleteAndPrint
};
