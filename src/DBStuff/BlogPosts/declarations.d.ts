// -------Types
import { DeepPartial } from 'typeorm';
import { Authors } from '../Authors/entity';

export interface IBlogPost {
  title: string;
  content: string;
  author: DeepPartial<Authors>;
}

export interface IBlogPostWithId {
  id: number;
  title?: string;
  content?: string;
  author?: Authors;
}

export interface IBlogPostSearch {
  id?: number;
  title?: string;
  content?: string;
  author?: DeepPartial<Authors>;
}
