import {
  Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne
} from 'typeorm';
import { Authors } from '../Authors/entity';

@Entity()
export class BlogPosts {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  content: string;

  @ManyToOne(() => Authors)
  @JoinColumn()
  author: Authors;
}
