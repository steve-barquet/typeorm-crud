// ---Types
import { IBlogPost, IBlogPostSearch, IBlogPostWithId } from './declarations';
// ---DB
import { blogPostsRepo } from '../configuration';

// ------------------------------Authors----------------------------------------------------
async function insert(incomingData: IBlogPost) {
  try {
    const newInsertion = blogPostsRepo.create(incomingData);
    const saveOeration = await blogPostsRepo.save(newInsertion);
    return {
      isError: false,
      data: saveOeration
    };
  } catch (error) {
    return {
      isError: true,
      data: error
    };
  }
}

async function readAll() {
  try {
    const allAuthors = await blogPostsRepo.find({ loadRelationIds: true });
    return {
      isError: false,
      data: allAuthors
    };
  } catch (error) {
    return {
      isError: true,
      data: error
    };
  }
}

async function edit(incomingData: IBlogPostWithId) {
  try {
    let targetBlog = await blogPostsRepo.findOneBy({ id: incomingData.id });

    if (targetBlog && targetBlog.title) {
      targetBlog = { ...targetBlog, ...incomingData };
      const updatedAuthor = await blogPostsRepo.save(targetBlog);
      return {
        isError: false,
        data: updatedAuthor
      };
    }
    throw new Error('No se encontró el Post');
  } catch (error) {
    return {
      isError: true,
      data: error
    };
  }
}

async function searchByParam(data: IBlogPostSearch) {
  try {
    const allAuthors = await blogPostsRepo.find({ where: data });
    return {
      isError: false,
      data: allAuthors
    };
  } catch (error) {
    return {
      isError: true,
      data: error
    };
  }
}

async function deleteBlog(id: number) {
  try {
    const result = await blogPostsRepo.delete(id);
    return {
      isError: false,
      data: result
    };
  } catch (error) {
    return {
      isError: true,
      data: error
    };
  }
}
export const blogPostDB = {
  getAll: readAll,
  getByProps: searchByParam,
  saveOne: insert,
  updateOne: edit,
  deleteOne: deleteBlog
};
