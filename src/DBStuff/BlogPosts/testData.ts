import { IBlogPost } from './declarations';
import { blogPostDB } from './operations';

export const blogPostsEx: IBlogPost[] = [
  {
    title: 'Learning typeorm',
    content: 'Ayuda!!!!!!! está bien perro éste pex',
    author: { id: 1 }
  },
  {
    title: 'Learning Javascript',
    content: 'Ayuda!!!!!!! está bien divertido éste pex',
    author: { id: 2 }
  },
  {
    title: 'Learning ansible',
    content: 'Ayuda!!!!!!! está bien inetresante éste pex',
    author: { id: 3 }
  },
  {
    title: 'Learning native',
    content: 'Ayuda!!!!!!! está bien desconocida éste pex',
    author: { id: 5 }
  },
  {
    title: 'Learning graphql',
    content: 'Ayuda!!!!!!! está bien facilita',
    author: { id: 5 }
  }
];
async function justPrint() {
  console.log('\n---BlogPosts---: ', await blogPostDB.getAll());
}

async function seacrhByPost() {
  console.log('\n---BlogPosts---: ', await blogPostDB.getByProps({ author: { id: 5 } }));
}

async function insertAndPrint() {
  console.log('\n---BlogPosts---: ', await blogPostDB.getAll());
  const insertion = await blogPostDB.saveOne(blogPostsEx[4]);
  console.log('\ninsertion: ', insertion);
  console.log('\n---BlogPosts---: ', await blogPostDB.getAll());
}

async function editAndPrint() {
  console.log('\n---BlogPosts---: ', await blogPostDB.getAll());
  const update = await blogPostDB.updateOne({ id: 2, content: 'facilito el tema' });
  console.log('\ninsertion: ', update);
  console.log('\n---BlogPosts---: ', await blogPostDB.getAll());
}

async function deleteAndPrint() {
  console.log('\n---BlogPosts---: ', await blogPostDB.getAll());
  const update = await blogPostDB.deleteOne(2);
  console.log('\ninsertion: ', update);
  console.log('\n---BlogPosts---: ', await blogPostDB.getAll());
}

export const blogPostTest = {
  justPrint,
  insertAndPrint,
  editAndPrint,
  seacrhByPost,
  deleteAndPrint
};
