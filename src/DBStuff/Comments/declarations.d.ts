// -------Types
import { DeepPartial } from 'typeorm';
import { BlogPosts } from '../BlogPosts/entity';

export interface IComments {
  name: string;
  content: string;
  postId: DeepPartial<BlogPosts>;
}

export interface ICommentsWithId {
  id: number;
  name?: string;
  content?: string;
  postId?: BlogPosts;
}

export interface ICommentsSearch {
  id?: number;
  name?: string;
  content?: string;
  postId?: DeepPartial<BlogPosts>;
}
