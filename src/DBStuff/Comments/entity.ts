/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne
} from 'typeorm';
import { BlogPosts } from '../BlogPosts/entity';

@Entity()
export class Comments {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  content: string;

  @ManyToOne(() => BlogPosts)
  @JoinColumn()
  postId: BlogPosts;
}
