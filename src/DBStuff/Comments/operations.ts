// ---Types
import { IComments, ICommentsSearch, ICommentsWithId } from './declarations';
// ---DB
import { commentsRepo } from '../configuration';

// ------------------------------Authors----------------------------------------------------
async function insert(incomingData: IComments) {
  try {
    const newInsertion = commentsRepo.create(incomingData);
    const saveOeration = await commentsRepo.save(newInsertion);
    return {
      isError: false,
      data: saveOeration
    };
  } catch (error) {
    return {
      isError: true,
      data: error
    };
  }
}

async function readAll() {
  try {
    const allAuthors = await commentsRepo.find({ loadRelationIds: true });
    return {
      isError: false,
      data: allAuthors
    };
  } catch (error) {
    return {
      isError: true,
      data: error
    };
  }
}

async function searchByParam(data: ICommentsSearch) {
  try {
    const allAuthors = await commentsRepo.find({ where: data });
    return {
      isError: false,
      data: allAuthors
    };
  } catch (error) {
    return {
      isError: true,
      data: error
    };
  }
}

async function edit(incomingData: ICommentsWithId) {
  try {
    let targetComment = await commentsRepo.findOneBy({ id: incomingData.id });

    if (targetComment && targetComment.name) {
      targetComment = { ...targetComment, ...incomingData };
      const updatedAuthor = await commentsRepo.save(targetComment);
      return {
        isError: false,
        data: updatedAuthor
      };
    }
    throw new Error('No se encontró el Post');
  } catch (error) {
    return {
      isError: true,
      data: error
    };
  }
}
async function deleteComment(id: number) {
  try {
    const result = await commentsRepo.delete(id);
    return {
      isError: false,
      data: result
    };
  } catch (error) {
    return {
      isError: true,
      data: error
    };
  }
}
export const commentstDB = {
  getAll: readAll,
  getByProps: searchByParam,
  saveOne: insert,
  updateOne: edit,
  deleteOne: deleteComment
};
