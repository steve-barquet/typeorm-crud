import { IComments } from './declarations';
import { commentstDB } from './operations';

export const blogPostsEx: IComments[] = [
  {
    name: 'Mawas',
    content: 'Estas hecho chit',
    postId: { id: 1 }
  },
  {
    name: 'Kroms',
    content: 'Rimko crud!!!!!!!',
    postId: { id: 3 }
  },
  {
    name: 'Angelito',
    content: 'Es que me dijo el mau',
    postId: { id: 3 }
  },
  {
    name: 'Diego',
    content: 'Dame de fruta!!!!',
    postId: { id: 5 }
  },
  {
    name: 'Bomtz',
    content: 'Vamos mi amor!!!!!',
    postId: { id: 5 }
  }
];
async function justPrint() {
  console.log('\n---Comments---: ', await commentstDB.getAll());
}

async function seacrhByPost() {
  console.log('\n---Comments---: ', await commentstDB.getByProps({ postId: { id: 3 } }));
}

async function insertAndPrint() {
  console.log('\n---Comments---: ', await commentstDB.getAll());
  const insertion = await commentstDB.saveOne(blogPostsEx[4]);
  console.log('\ninsertion: ', insertion);
  console.log('\n---Comments---: ', await commentstDB.getAll());
}

async function editAndPrint() {
  console.log('\n---Comments---: ', await commentstDB.getAll());
  const update = await commentstDB.updateOne({ id: 2, content: 'facilito el tema' });
  console.log('\ninsertion: ', update);
  console.log('\n---Comments---: ', await commentstDB.getAll());
}

async function deleteAndPrint() {
  console.log('\n---Comments---: ', await commentstDB.getAll());
  const update = await commentstDB.deleteOne(2);
  console.log('\ninsertion: ', update);
  console.log('\n---Comments---: ', await commentstDB.getAll());
}

export const commentsTest = {
  justPrint,
  insertAndPrint,
  editAndPrint,
  seacrhByPost,
  deleteAndPrint
};
