import 'reflect-metadata';
import { DataSource } from 'typeorm';
// ---Entities
import { Authors } from './Authors/entity';
import { BlogPosts } from './BlogPosts/entity';
import { Comments } from './Comments/entity';

// -----------------Conecction-----------------
export const AppDataSource = new DataSource({
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'kros',
  password: 'comic456',
  database: 'fake_blog2',
  entities: [Comments, Authors, BlogPosts],
  synchronize: true,
  logging: false
});

// -----------------Repositories-----------------
export const authorsRepo = AppDataSource.getRepository(Authors);
export const commentsRepo = AppDataSource.getRepository(Comments);
export const blogPostsRepo = AppDataSource.getRepository(BlogPosts);
