// ---Configurations
import { AppDataSource } from './DBStuff/configuration';
// ---Routes
import { commentsTest } from './DBStuff/Comments/testData';
import { autorTest } from './DBStuff/Authors/testData';
import { blogPostTest } from './DBStuff/BlogPosts/testData';

async function main() {
  console.log('---------- Cool Programing ----------\n');

  try {
    // ---Conection singleton
    await AppDataSource.initialize();

    // ---DB Operations Testing
    await commentsTest.justPrint();
    await autorTest.justPrint();
    await blogPostTest.justPrint();
  } catch (error) {
    console.log('Error cachado: ', error);
  }
}

main();
